// Copyright 2019 Radiologics, Inc
// Developer: Kate Alpert <kate@radiologics.com>

package org.nrg.xnatx.plugins.batch.repositories;

import com.google.common.collect.ImmutableMap;
import org.intellij.lang.annotations.Language;
import org.nrg.action.ClientException;
import org.nrg.xdat.om.*;
import org.nrg.xdat.security.helpers.Groups;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.FieldNotFoundException;
import org.nrg.xft.exception.XFTInitException;
import org.nrg.xnatx.plugins.batch.model.Workflow;
import org.nrg.xnatx.plugins.batch.xapi.PageRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.security.UserI;
import org.restlet.data.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSetMetaData;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.*;

@SuppressWarnings({"SqlNoDataSourceInspection", "SqlResolve"})
@Slf4j
@Repository
public class WorkflowRepository implements PageableRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    private static final String WRK_FIELDS = " wrk.wrk_workflowdata_id, wrk.id, wrk.externalid, wrk.pipeline_name, " +
            "wrk.data_type, wrk.details, wrk.launch_time, wrk.status, wrk.step_description, wrk.percentagecomplete, " +
            "wrk.comments, wrk.justification, last_modified, u.login AS create_user ";

    private static final Map<String, ColumnDataType> COLUMN_INFO = ImmutableMap.<String, ColumnDataType>builder()
            .put("wrk_workflowdata_id", new ColumnDataType("wfid", int.class))
            .put("id", new ColumnDataType("id", String.class))
            .put("label", new ColumnDataType("label", String.class))
            .put("externalid", new ColumnDataType("externalId", String.class))
            .put("pipeline_name", new ColumnDataType("pipelineName", String.class))
            .put("data_type", new ColumnDataType("dataType", String.class))
            .put("details", new ColumnDataType("details", String.class))
            .put("launch_time", new ColumnDataType("launchTime", Timestamp.class))
            .put("status", new ColumnDataType("status", String.class))
            .put("step_description", new ColumnDataType("stepDescription", String.class))
            .put("percentagecomplete", new ColumnDataType("percentageComplete", String.class))
            .put("comments", new ColumnDataType("comments", String.class))
            .put("justification", new ColumnDataType("justification", String.class))
            .put("last_modified", new ColumnDataType("modTime", Timestamp.class))
            .put("create_user", new ColumnDataType("createUser", String.class))
            .build();

    private static final RowMapper<Workflow> WF_ROW_MAPPER = (resultSet, index) -> {
        ResultSetMetaData metaData = resultSet.getMetaData();
        Workflow w = new Workflow();
        for (int i = 1; i <= metaData.getColumnCount(); i++) {
            String columnName = metaData.getColumnName(i);
            Object item = resultSet.getObject(columnName);
            if (!COLUMN_INFO.containsKey(columnName) || item == null) {
                continue;
            }
            ColumnDataType cdt = COLUMN_INFO.get(columnName);
            Class<?> columnClass = cdt.dataType;
            if (columnClass.equals(Timestamp.class)) {
                item = new Date(((Timestamp) item).getTime());
                columnClass = Date.class;
            }
            w.setProperty(cdt.columnName, item, columnClass);
        }
        return w;
    };

    @Language("SQL")
    public static final String QUERY_WFS = "SELECT " + WRK_FIELDS + ", COALESCE(expt.label, subj.label, wrk.id) AS label " +
            "           FROM wrk " +
            "               LEFT JOIN xnat_experimentData expt ON wrk.id = expt.id " +
            "               LEFT JOIN xnat_subjectData subj ON wrk.id = subj.id " +
            "               LEFT JOIN wrk_workflowdata_meta_data meta ON wrk.workflowData_info = meta.meta_data_id " +
            "               LEFT JOIN xdat_user u ON meta.insert_user_xdat_user_id = u.xdat_user_id ";


    // SQL from WorkflowBasedHistoryBuilder
    // Pipelines on subject
    @Language("SQL")
    public static final String QUERY_SUBJECT_WFS = "SELECT " + WRK_FIELDS + ", s.label " +
            "FROM (SELECT * FROM wrk_workflowData WHERE id = :id OR " +
            "id IN (SELECT DISTINCT id FROM (SELECT sad.id FROM xnat_subjectassessordata sad WHERE subject_id=:id " +
            "UNION SELECT iad.id FROM xnat_subjectassessordata sad LEFT JOIN xnat_imageassessordata iad " +
            "ON sad.id=iad.imagesession_id WHERE iad.id IS NOT NULL AND subject_id=:id UNION " +
            "SELECT sad.id FROM xnat_subjectassessordata_history sad WHERE subject_id=:id UNION " +
            "SELECT iad.id FROM xnat_subjectassessordata sad LEFT JOIN xnat_imageassessordata_history iad " +
            "ON sad.id=iad.imagesession_id WHERE iad.id IS NOT NULL AND subject_id=:id UNION " +
            "SELECT iad.id FROM xnat_subjectassessordata_history sad LEFT JOIN xnat_imageassessordata_history iad " +
            "ON sad.id=iad.imagesession_id WHERE iad.id IS NOT NULL AND subject_id=:id) AS idq)) AS wrk " +
            "INNER JOIN xnat_subjectdata s ON wrk.id=s.id " +
            "LEFT JOIN xnat_subjectdata_meta_data md ON s.subjectdata_info = md.meta_data_id " +
            "LEFT JOIN wrk_workflowdata_meta_data meta ON wrk.workflowData_info = meta.meta_data_id " +
            "LEFT JOIN xdat_user u ON meta.insert_user_xdat_user_id = u.xdat_user_id ";

    // Pipelines on experiment
    @Language("SQL")
    public static final String QUERY_EXPT_WFS = "SELECT " + WRK_FIELDS + ", xnat_experimentdata.label " +
            "FROM (SELECT * FROM wrk_workflowData WHERE id = :id OR " +
            "id IN (SELECT DISTINCT id FROM (SELECT iad.id FROM xnat_imageassessordata iad " +
            "WHERE iad.id IS NOT NULL AND iad.imagesession_id=:id UNION " +
            "SELECT iad.id FROM xnat_imageassessordata_history iad " +
            "WHERE iad.id IS NOT NULL AND iad.imagesession_id=:id) AS idq)) as wrk " +
            "INNER JOIN xnat_experimentdata ON wrk.id=xnat_experimentdata.id " +
            "LEFT JOIN wrk_workflowdata_meta_data meta ON wrk.workflowData_info=meta.meta_data_id " +
            "LEFT JOIN xdat_user u ON meta.insert_user_xdat_user_id = u.xdat_user_id ";

    private static final String PROJECT_PLINE_DATATYPES = "'" + ArcProject.SCHEMA_ELEMENT_NAME + "','" +
            XnatProjectdata.SCHEMA_ELEMENT_NAME + "'";

    @Autowired
    public WorkflowRepository(final NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Set<String> getAllowableSortColumns() {
        return COLUMN_INFO.keySet();
    }
    public Set<String> getAllowableFilterColumns() {
        return COLUMN_INFO.keySet();
    }

    public Map<String, ColumnDataType> getColumnMapping() {
        return COLUMN_INFO;
    }

    /**
     * Get list of model
     *
     * @param id        item id
     * @param dataType  item type
     * @param user      user (no permissions checking, just used if item type == xdat:user)
     * @param request   the request object
     * @return list of model
     * @throws DataAccessException for issues accessing data
     * @throws Exception for issues retrieving xnat data types
     */
    public List<Workflow> getWorkflows(String id, String dataType, UserI user,
                                       PageRequest request) throws Exception {

        MapSqlParameterSource namedParams = new MapSqlParameterSource();

        if (StringUtils.isNotBlank(id)) {
            namedParams.addValue("id", id);
        }

        String query;
        switch (dataType) {
            case "xdat:user":
                if (request.isAdminWorkflows()) {
                    if (!Groups.isSiteAdmin(user)) {
                        throw new ClientException(Status.CLIENT_ERROR_FORBIDDEN, "Must be site admin");
                    }
                    query = setupQueryWfs(0) + QUERY_WFS + " WHERE wrk.externalid = '" + PersistentWorkflowUtils.ADMIN_EXTERNAL_ID + "'";
                } else {
                    query = makeUserLevelQuery(user, namedParams, request.getLimit(), request.getOffset(),
                            request.isSortable(), request.getDays());
                }
                break;
            case XnatProjectdata.SCHEMA_ELEMENT_NAME:
                //TODO support days for project, subject, session
                query = setupQueryWfs(0) + QUERY_WFS + " WHERE wrk.externalid = '" + id + "'";
                break;
            case XnatSubjectdata.SCHEMA_ELEMENT_NAME:
                query = QUERY_SUBJECT_WFS;
                break;
            default:
                query = QUERY_EXPT_WFS;
                break;
        }
        if (request.isSortable()) {
            query = "SELECT * FROM (" + query + ") AS q"; //Allow for WHERE in query suffix
            query += request.getQuerySuffix(namedParams);
        }

        return jdbcTemplate.query(query, namedParams, WF_ROW_MAPPER);
    }

    private String setupQueryWfs(int days) {
        String whereClause = days > 0 ? " WHERE launch_time > (NOW() - INTERVAL '" + days + " days') " : "";
        return "WITH wrk AS (SELECT * FROM wrk_workflowdata " + whereClause + ") ";
    }

    /**
     * Make query to retrieve workflows for user dashboard:
     *  - Workflows user has launched and workflows associated with data user can read
     *  - if site admin: the above plus all workflows with ADMIN externalId
     *  - if all data admin: return all workflows
     *
     * @param user the user
     * @param namedParams the map SQL params
     * @return the query
     */
    @Language("SQL")
    private String makeUserLevelQuery(UserI user,
                                      MapSqlParameterSource namedParams,
                                      int limit,
                                      int offset,
                                      boolean sortable,
                                      int days)  {

        namedParams.addValue("rowLimit", limit)
                .addValue("rowOffset", offset);

        @Language("SQL") String query;

        @Language("SQL") String limitSuffix = "";
        if (!sortable) {
            limitSuffix = " ORDER BY wrk_workflowdata_id DESC LIMIT :rowLimit OFFSET :rowOffset";
        }

        if (Groups.isDataAdmin(user)) {
            // Access to all data workflows across the whole site
            query = setupQueryWfs(days) + QUERY_WFS;
            if (!Groups.isSiteAdmin(user)) {
                query += " WHERE externalid != '" + PersistentWorkflowUtils.ADMIN_EXTERNAL_ID + "' ";
            }
            query += limitSuffix;
        } else {

            @Language("SQL") String daysSuffix = "";
            if (days > 0) {
                daysSuffix = " AND launch_time > NOW() - INTERVAL '" + days + " days' ";
            }

            namedParams.addValue("userId", user.getID())
                    .addValue("username", user.getLogin());

            // Workflows associated with data user can read (or edit in the case of projects)
            query = "WITH permSub1 AS (SELECT * FROM " +
                    "(SELECT " +
                    "   REGEXP_REPLACE(REGEXP_REPLACE(m.field, '(/project|/ID)$', ''), '/sharing/share$', '') AS data_type, " +
                    "   m.field_value  AS project, " +
                    "   m.read_element AS read, " +
                    "   m.edit_element AS edit " +
                    "       FROM xdat_field_mapping m " +
                    "          LEFT JOIN xdat_field_mapping_set s ON m.xdat_field_mapping_set_xdat_field_mapping_set_id = s.xdat_field_mapping_set_id " +
                    "          LEFT JOIN xdat_element_access a ON s.permissions_allow_set_xdat_elem_xdat_element_access_id = a.xdat_element_access_id " +
                    "          LEFT JOIN xdat_usergroup g ON a.xdat_usergroup_xdat_usergroup_id = g.xdat_usergroup_id " +
                    "          LEFT JOIN xdat_user_groupid i ON g.id = i.groupid " +
                    "          LEFT JOIN xdat_user u ON i.groups_groupid_xdat_user_xdat_user_id = u.xdat_user_id " +
                    "       WHERE u.login = :username AND (m.read_element = 1 OR m.edit_element = 1)) AS ps " +
                    "       GROUP BY data_type, project, read, edit)," +
                    "permSub2 AS (SELECT 'arc:project'::varchar AS data_type, " +
                    "   ap.arc_project_id::varchar AS project, " +
                    "   permSub1.read, " +
                    "   permSub1.edit " +
                    "       FROM permSub1 " +
                    "           INNER JOIN arc_project ap ON permSub1.data_type = '" + XnatProjectdata.SCHEMA_ELEMENT_NAME + "' " +
                    "               AND permSub1.project = ap.id), " +
                    "permSubQ AS (SELECT * FROM permSub1 UNION SELECT * FROM permSub2), " +
                    "wrk AS (SELECT w.* FROM permSubQ " +
                    "               LEFT JOIN wrk_workflowdata w " +
                    "                   ON (" +
                    "                    (permSubQ.data_type IN (" + PROJECT_PLINE_DATATYPES + ") AND permSubQ.edit = 1 AND " +
                    "                          permSubQ.data_type = w.data_type AND permSubQ.project = w.id) " +
                    "                      OR " +
                    "                      (permSubQ.data_type NOT IN (" + PROJECT_PLINE_DATATYPES + ") AND " +
                    "                           permSubQ.data_type = w.data_type AND permSubQ.project = w.externalid) " +
                    "                  )"  +
                    "              WHERE w.wrk_workflowData_id IS NOT NULL " + daysSuffix +
                    "         GROUP BY w.wrk_workflowData_id " + limitSuffix +
                    ") " + QUERY_WFS;
        }
        return query;
    }

    /**
     * Get Workflow model object from PersistentWorkflowI object
     * @param wrk   PersistentWorkflowI object
     * @param user  user
     * @return      Worflow model object
     */
    public Workflow getWorkflow(PersistentWorkflowI wrk, UserI user) {
        String label;
        Date lastMod = null;
        try {
            lastMod = ((WrkWorkflowdata) wrk).getItem().getMeta().getDateProperty("last_modified");
        } catch (XFTInitException | ElementNotFoundException | FieldNotFoundException | ParseException e) {
            // Ignore exceptions and just leave mod time null
            log.error("Unable to retrieve last modified time from workflow meta data", e);
        }

        switch (wrk.getDataType()) {
            case XnatProjectdata.SCHEMA_ELEMENT_NAME:
                XnatProjectdata proj = XnatProjectdata.getXnatProjectdatasById(wrk.getId(), user, false);
                label = proj == null ? null : proj.getId();
                break;
            case XnatSubjectdata.SCHEMA_ELEMENT_NAME:
                XnatSubjectdata subj = XnatSubjectdata.getXnatSubjectdatasById(wrk.getId(), user, false);
                label = subj == null ? null : subj.getLabel();
                break;
            default:
                XnatExperimentdata exp = XnatExperimentdata.getXnatExperimentdatasById(wrk.getId(), user, false);
                label = exp == null ? null : exp.getLabel();
                break;
        }

        String createUser = wrk.getCreateUser();
        if (createUser == null) {
            createUser = ((WrkWorkflowdata) wrk).getUser().getLogin();
        }

        return new Workflow(wrk.getWorkflowId(), wrk.getId(), label, wrk.getExternalid(), wrk.getPipelineName(),
                wrk.getDataType(), wrk.getDetails(), wrk.getLaunchTimeDate(), wrk.getStatus(), wrk.getStepDescription(),
                wrk.getPercentagecomplete(), wrk.getComments(), wrk.getJustification(), lastMod, createUser);
    }
}
